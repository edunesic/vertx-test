package com.example.demo;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.redis.RedisClient;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.RedisDataSource;

public class MainVerticle extends AbstractVerticle {

    RedisClient client;
    private final static String KEY = "COURSE";

    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());
        router.get("/course").handler(this::getList);
        router.post("/course").handler(this::addList);
        router.put("/course/:id").handler(this::changeList);
        router.delete("/course/:id").handler(this::deleteList);

        ServiceDiscovery.create(vertx, discovery -> {
            System.out.println(discovery.bindings());
            RedisDataSource.getRedisClient(discovery,
                svc -> svc.getName().equals("redis"),
                ar -> {
                    if (ar.failed()) {
                        System.out.println("D'oh !");
                    } else {
                        client = ar.result();
                        vertx.createHttpServer()
                            .requestHandler(router::accept)
                            .listen(8080);
                    }
                });
        });
    }

    private void changeList(RoutingContext rc) {
    }

    private void deleteList(RoutingContext rc) {
        Integer id = Integer.parseInt(rc.pathParam("id"));
        client.hdel(KEY, id.toString(), l -> {
            getList(rc);
        });
    }

    private void addList(RoutingContext rc) {
        JsonObject json = rc.getBodyAsJson();
        String name = json.getString("name", "default");
        String lesson = json.getString("lesson", "default");

        client.hset(KEY, name, lesson, l -> getList(rc));
    }

    private void getList(RoutingContext rc) {
        client.hgetall(KEY, json -> {
            if (json.failed()) {
                rc.fail(json.cause());
            } else {
                rc.response()
                    .putHeader("X-Served-By", System.getenv("HOSTNAME"))
                    .end(json.result().encodePrettily());
            }
        });
    }
}
